"use strict";

const { readFileSync } = require("fs");
const { default: createStrapi } = require("strapi");
const { CLIENT_RENEG_LIMIT } = require("tls");

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html#bootstrap
 */

module.exports = () => 
{
  const mysql = require("mysql");
  const db = mysql.createConnection(
    {
    database: "test-strapi",
    host: "localhost",
    user: "root",
    password: "AZERty1234",
    });
  db.connect(function (err) 
  
    {
    if (err) throw err;
    console.log("Connecté à la base de données MySQL!");
    function addCategories() 
    {
        let bddCategoriesArray = [];
        let bddCategories = [];
        let categoriesJson = [];
        db.query("SELECT Name FROM categories", function (err, result) 
        {
        if (err) 
        {
            throw err;
        } else 
        {
            setValue(result);
        }
        });
        function setValue(value) 
        {
        bddCategories = value;
        bddCategoriesArray = bddCategories.map((category) => 
        {
            return category.Name;
        });
        const categoriesRead = readFileSync("./data/categories.json");
        const categoriesData = JSON.parse(categoriesRead);
        let entriesCategoriesJson = categoriesData.categories.map((entries) => 
        {
            return entries;
        });
        categoriesJson = categoriesData.categories.map((entries) => 
        {
            return entries.name;
        });
        let searchCategoriesDifference = categoriesJson.filter(
            (x) => !bddCategoriesArray.includes(x)
        );
        let categoriesDifference = entriesCategoriesJson.filter(
            (x) => x.name == searchCategoriesDifference[0]
        );
        if (searchCategoriesDifference != "") {
            return strapi.services.categories.create(
            {
            Name: categoriesDifference[0].name,
            Icon: categoriesDifference[0].icon,
            Alt: categoriesDifference[0].alt,
            Markercolor: categoriesDifference[0].markerColor,
            });
        }
        }
    }
    function addLanguages()
    {
        db.query("SELECT Name FROM languages", function (err, result) 
        {
        if (err) 
        {
            throw err;
        } else 
        {
            setValue(result);
        }
        });
        function setValue(value) 
        {
            let languagesJson = [];
            let bddLanguagesArray = [];
            let bddLanguages = [];
            bddLanguages = value;
            bddLanguagesArray = bddLanguages.map((language) => 
            {
                return language.Name;
            });
            const languagesRead = readFileSync("./data/languages.json");
            const languagesData = JSON.parse(languagesRead);
            
            let entriesLanguagesJson = languagesData.languages.map((entries)=>
            {
                return entries;
            })
            languagesJson = languagesData.languages.map((entries)=>
            {
                return entries.name
            })
            let searchLanguagesDifference = languagesJson.filter(
                (x)=> !bddLanguagesArray.includes(x)
            );
            let languagesDifference = entriesLanguagesJson.filter(
                (x) => x.name == searchLanguagesDifference[0]
            );
            if(searchLanguagesDifference != "")
            {
                return strapi.services.languages.create(
                    {
                        Name: languagesDifference[0].name,
                        Icon: languagesDifference[0].icon
                    });
            }
        }
    }
    function addPlaces() {
        db.query("SELECT Name, Category FROM places", function (err, result) 
        {
        if (err) 
        {
            throw err;
        } else 
        {
            setValue(result);
        }
        });
        function setValue(value) 
        {
            let placesJson = [];
            let bddPlacesArray = [];
            let bddPlaces = [];
            bddPlaces = value;
            bddPlacesArray = bddPlaces.map((place) => 
            {
                return place.Name + " " + place.Category;
            });
            const placesRead = readFileSync("./data/places.json");
            const placesData = JSON.parse(placesRead);
            let entriesPlacesJson = placesData.places.map((entries)=>
            {
                return entries;
            })
            placesJson = placesData.places.map((entries)=>
            {
                return entries.Name + " " + entries.Category
            })
            let searchPlacesDifference = placesJson.filter(
                (x)=> !bddPlacesArray.includes(x)
            );
            
            let placesDifference = entriesPlacesJson.filter(
                (x) => x.Name + " " + x.Category == searchPlacesDifference[0]
            );
               
            if(searchPlacesDifference != "")
            {
                    return strapi.services.places.create(
                        {
                            Adress: placesDifference[0].Adress,
                            Category: placesDifference[0].Category,
                            Latitude: placesDifference[0].Latitude,
                            Longitude: placesDifference[0].Longitude,
                            Name: placesDifference[0].Name,
                            Phone: placesDifference[0].Phone,
                            Timetable: placesDifference[0].Timetable,
                            Weblink: placesDifference[0].Weblink

                        });
            }
        }
    }
    function addUsefullNumbers()
    {
        db.query("SELECT Name FROM usefullnumbers", function (err, result) 
        {
        if (err) 
        {
            throw err;
        } else 
        {
            setValue(result);
        }
        });
        
        function setValue(value) 
        {
            
            let usefullNumbersJson = [];
            let bddUsefullNumbersArray = [];
            let bddUsefullNumbers = [];
            bddUsefullNumbers = value;
            bddUsefullNumbersArray = bddUsefullNumbers.map((number) => 
            {
                return number.Name;
            });
            const usefullNumbersRead = readFileSync("./data/numbers.json");
            const usefullNumbersData = JSON.parse(usefullNumbersRead);
            let entriesUsefullNumbersJson = usefullNumbersData.numbers.map((entries)=>
            {
                return entries;
            })
            usefullNumbersJson = usefullNumbersData.numbers.map((entries)=>
            {
                return entries.name
            })
            let searchUsefullNumbersDifference = usefullNumbersJson.filter(
                (x)=> !bddUsefullNumbersArray.includes(x)
            );
            let usefullNumbersDifference = entriesUsefullNumbersJson.filter(
                (x) => x.name == searchUsefullNumbersDifference[0]
            );
            
            if(searchUsefullNumbersDifference != "")
            {
                return strapi.services.usefullnumbers.create(
                    {
                        Name: usefullNumbersDifference[0].name,
                        Logo: usefullNumbersDifference[0].logo,
                        Phone: usefullNumbersDifference[0].tel,
                        Alt: usefullNumbersDifference[0].alt,
                        Sector: usefullNumbersDifference[0].sector

                    });
            }
        }
    }
    function addWords()
    {
        db.query("SELECT Name, Role, Languageswords FROM words", function (err, result) 
        {
        if (err) 
        {
            throw err;
        } else 
        {
            setValue(result);
        }
        });
        
        function setValue(value) 
        {
            
            let wordsJson = [];
            let bddWordsArray = [];
            let bddWords = [];
            bddWords = value;
            bddWordsArray = bddWords.map((word) => 
            {
                return word.Name + " " + word.Role + " " + word.Languageswords;
            });
            
            const wordsRead = readFileSync("./locales/kr/translate.json");
            const wordsData = JSON.parse(wordsRead);
            let entriesWordsJson = wordsData.word.map((entries)=>
            {
                return entries;
            })
            wordsJson = wordsData.word.map((entries)=>
            {
                return entries.name + " " + entries.role + " " + entries.languagesWords
            })
            let searchWordsDifference = wordsJson.filter(
                (x)=> !bddWordsArray.includes(x)
            );
            let wordsDifference = entriesWordsJson.filter(
                (x) => x.name + " " + x.role + " " + x.languagesWords == searchWordsDifference[0]
            );
            console.log(searchWordsDifference.length)
            if(searchWordsDifference.length != 0)
            {
                   return strapi.services.words.create(
                       {
                           Name: wordsDifference[0].name,
                           Role: wordsDifference[0].role,
                           Languageswords: wordsDifference[0].languagesWords
                        

                       });
                
            }
        }
    }
addCategories(),addLanguages(),addPlaces(),addUsefullNumbers();addWords()
}
);


}
    
  
    

